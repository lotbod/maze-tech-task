using Game.Player;
using Maze;
using Zenject;

namespace Installers
{
    public class GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<MazeBuilder>().AsSingle();
            Container.BindInterfacesTo<PlayerHandler>().AsSingle();
        }
    }
}