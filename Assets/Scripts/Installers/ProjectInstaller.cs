using Maze.Saver.Gateways;
using Maze.Saver.UseCases;
using Zenject;

namespace Installers
{
    public class ProjectInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<SaveGateway>().AsSingle();

            Container.Bind<SaveLevelUseCase>().AsTransient();
            Container.Bind<GetLevelsNamesUseCase>().AsTransient();
            Container.Bind<TryToLoadLevelUseCase>().AsTransient();
        }
    }
}