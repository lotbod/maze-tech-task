using Core.UiSystem;
using Core.UiSystem.Data;
using Core.UiSystem.Interfaces;
using Core.UiSystem.Pool;
using Cysharp.Threading.Tasks;
using Game.Data;
using Game.Effects;
using Game.Map;
using Game.Player;
using Maze.Data;
using Maze.Saver.UseCases;
using UiWindows.Views;
using UnityEngine;

namespace UiWindows.Controllers
{
    public class MazeGameWindow : WindowController<MazeGameView>
    {
        private readonly IMapHandler mapHandler;
        private readonly SaveLevelUseCase saveLevelUseCase;
        private readonly IEffectsHandler effectsHandler;
        private readonly IPlayerHandler playerHandler;
        
        private MazeLevelData currentLevelData;
        private bool isTimerRunning;
        private float elapsedTime;
        private int totalSteps;

        public MazeGameWindow(WindowType type, IWindowHandler windowHandler, ViewPool viewPool, IMapHandler mapHandler, 
            SaveLevelUseCase saveLevelUseCase, IEffectsHandler effectsHandler, IPlayerHandler playerHandler) 
            : base(type, windowHandler, viewPool)
        {
            this.mapHandler = mapHandler;
            this.saveLevelUseCase = saveLevelUseCase;
            this.effectsHandler = effectsHandler;
            this.playerHandler = playerHandler;
        }

        protected override void OnOpen(object data = null)
        {
            currentLevelData = (MazeLevelData) data!;
            ResetWindow();
            
            playerHandler.OnStart += StartTimer;
            playerHandler.OnEnd += HandleEnd;
            playerHandler.OnStep += HandlePlayerStep;
            ConcreteView.OnClick += HandleClick;
        }

        protected override void OnClose()
        {
            mapHandler.ClearMap();
            effectsHandler.StopEffect(ParticleType.MazeBackground);
                
            playerHandler.OnStart -= StartTimer;
            playerHandler.OnEnd -= HandleEnd;
            playerHandler.OnStep -= HandlePlayerStep;
            ConcreteView.OnClick -= HandleClick;
        }

        private void ResetWindow()
        {
            mapHandler.BuildMap(currentLevelData);
            effectsHandler.PlayEffect(ParticleType.MazeBackground);
            elapsedTime = 0;
            totalSteps = 0;
            ConcreteView.UpdateTimer(elapsedTime);
            ConcreteView.UpdateSteps(totalSteps);
        }

        private void HandleClick(MazeGameView.BtnType btnType)
        {
            switch (btnType)
            {
                case MazeGameView.BtnType.Back:
                    HandleBackClick();
                    break;
                case MazeGameView.BtnType.SaveLevel:
                    HandleSaveClick();
                    break;
            }
        }

        private void HandleBackClick()
        {
            WindowHandler.OpenWindow(WindowType.LevelChooser);
            Close();
        }
        
        private void HandleSaveClick()
        {
            saveLevelUseCase.SaveLevel(currentLevelData);
            ConcreteView.StopSaveInteract();
        }
        
        private void StartTimer()
        {
            isTimerRunning = true;
            TimerCoroutine().Forget();
        }

        private void HandleEnd()
        {
            isTimerRunning = false;

            WindowHandler.OpenWindow(WindowType.EndGame, (currentLevelData, new LevelStatsData
            {
                ElapsedTime = elapsedTime, 
                TotalSteps = totalSteps
            }));
        }

        private void HandlePlayerStep()
        {
            totalSteps++;
            ConcreteView.UpdateSteps(totalSteps);
        }
        
        private async UniTask TimerCoroutine()
        {
            while (isTimerRunning)
            {
                elapsedTime += Time.deltaTime;
                ConcreteView.UpdateTimer(elapsedTime);
                await UniTask.Yield();
            }
        }
    }
}