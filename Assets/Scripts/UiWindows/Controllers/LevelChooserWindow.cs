using Core.UiSystem;
using Core.UiSystem.Data;
using Core.UiSystem.Interfaces;
using Core.UiSystem.Pool;
using Cysharp.Threading.Tasks;
using Maze;
using Maze.Interfaces;
using Maze.Saver.UseCases;
using UiWindows.Views;

namespace UiWindows.Controllers
{
    public class LevelChooserWindow : WindowController<LevelChooserView>
    {
        private readonly IMazeBuilder mazeBuilder;
        private readonly GetLevelsNamesUseCase getLevelsNamesUseCase;
        private readonly TryToLoadLevelUseCase tryToLoadLevelUseCase;

        private string[] loadedLevelsNames;
        private int currentLevelIndex;
        private int currentWidth;
        private int currentHeight;
        private int currentExits = 1;

        public LevelChooserWindow(WindowType type, IWindowHandler windowHandler, ViewPool viewPool,
            IMazeBuilder mazeBuilder, GetLevelsNamesUseCase getLevelsNamesUseCase, TryToLoadLevelUseCase tryToLoadLevelUseCase) 
            : base(type, windowHandler, viewPool)
        {
            this.mazeBuilder = mazeBuilder;
            this.getLevelsNamesUseCase = getLevelsNamesUseCase;
            this.tryToLoadLevelUseCase = tryToLoadLevelUseCase;
        }

        protected override void OnOpen(object data = null)
        {
            ShowLevels();
            ConcreteView.OnClick += HandlePlayRandomClick;
            ConcreteView.OnSlider += HandleSliderChange;
        }

        protected override void OnClose()
        {
            ConcreteView.OnClick -= HandlePlayRandomClick;
            ConcreteView.OnSlider -= HandleSliderChange;
        }

        private void ShowLevels()
        {
            loadedLevelsNames = getLevelsNamesUseCase.GetNames();

            if (loadedLevelsNames.Length > 0)
            {
                ConcreteView.SetLevelName(loadedLevelsNames[currentLevelIndex]);
                ConcreteView.EnableLevelsUi();
                CheckArrowsAvailability();
                return;
            }

            ConcreteView.DisableLevelsUi();
            ConcreteView.SetLevelName("No levels to load.");
        }

        private void HandlePlayRandomClick(LevelChooserView.BtnType btnType)
        {
            switch (btnType)
            {
                case LevelChooserView.BtnType.Random:
                    HandleRandomClick();
                    break;
                case LevelChooserView.BtnType.ArrowLeft:
                    HandleLeftArrowClick();
                    break;
                case LevelChooserView.BtnType.ArrowRight:
                    HandleRightArrowClick();
                    break;
                case LevelChooserView.BtnType.PlaySelected:
                    HandlePlaySelectedClick();
                    break;
            }
        }

        private void HandleSliderChange(LevelChooserView.SliderType inputType, float value, float minValue)
        {
            int intValue = (int) value;
            int intMinValue = (int) minValue;

            switch (inputType)
            {
                case LevelChooserView.SliderType.Width:
                    if (intValue == intMinValue) intValue = 0;
                    currentWidth = intValue;
                    ConcreteView.SetSliderText(LevelChooserView.SliderType.Width, currentWidth > 0 ? currentWidth.ToString() : "RND");
                    break;
                case LevelChooserView.SliderType.Height:
                    if (intValue == intMinValue) intValue = 0;
                    currentHeight = intValue;
                    ConcreteView.SetSliderText(LevelChooserView.SliderType.Height, currentHeight > 0 ? currentHeight.ToString() : "RND");
                    break;
                case LevelChooserView.SliderType.Exits:
                    currentExits = intValue;
                    ConcreteView.SetSliderText(LevelChooserView.SliderType.Exits, currentExits.ToString());
                    break;
            }
        }

        private void HandleRandomClick()
        {
            var mazeLevel = mazeBuilder.GenerateMaze(new MazeConfiguration
            {
                MazeWidth = currentWidth,
                MazeHeight = currentHeight,
                ExitsCount = currentExits
            });

            WindowHandler.OpenWindow(WindowType.MazeGame, mazeLevel);
            Close();
        }

        private void HandleLeftArrowClick()
        {
            if (currentLevelIndex <= 0) return;

            currentLevelIndex--;
            ConcreteView.SetLevelName(loadedLevelsNames[currentLevelIndex]);
            CheckArrowsAvailability();
        }
        
        private void HandleRightArrowClick()
        {
            if (currentLevelIndex >= loadedLevelsNames.Length - 1) return;

            currentLevelIndex++;
            ConcreteView.SetLevelName(loadedLevelsNames[currentLevelIndex]);
            CheckArrowsAvailability();
        }

        private void CheckArrowsAvailability()
        {
            ConcreteView.SetButtonActive(LevelChooserView.BtnType.ArrowLeft, currentLevelIndex != 0);
            ConcreteView.SetButtonActive(LevelChooserView.BtnType.ArrowRight, currentLevelIndex < loadedLevelsNames.Length - 1);
        }

        private void HandlePlaySelectedClick()
        {
            if (!tryToLoadLevelUseCase.TryToLoadLevel(loadedLevelsNames[currentLevelIndex], out var mazeLevel))
            {
                HandleLevelLoadError();
                return;
            }
            
            WindowHandler.OpenWindow(WindowType.MazeGame, mazeLevel);
            Close();
        }

        private async void HandleLevelLoadError()
        {
            ConcreteView.SetLevelName("LoadLevelError");
            await UniTask.Delay(1500);
            ConcreteView.SetLevelName(loadedLevelsNames[currentLevelIndex]);
        }
    }
}