using Core.UiSystem;
using Core.UiSystem.Data;
using Core.UiSystem.Interfaces;
using Core.UiSystem.Pool;
using Game.Data;
using Game.Effects;
using Maze.Data;
using UiWindows.Views;

namespace UiWindows.Controllers
{
    public class EndGameWindow : WindowController<EndGameView>
    {
        private readonly IEffectsHandler effectsHandler;
        private MazeLevelData currentLevelData;
        private LevelStatsData levelStatsData;
        
        public EndGameWindow(WindowType type, IWindowHandler windowHandler, ViewPool viewPool, 
            IEffectsHandler effectsHandler)
            : base(type, windowHandler, viewPool)
        {
            this.effectsHandler = effectsHandler;
        }

        protected override void OnOpen(object data = null)
        {
            var pair =  ((MazeLevelData, LevelStatsData)) data!;

            currentLevelData = pair.Item1;
            levelStatsData = pair.Item2;

            InitializeView();
            
            ConcreteView.OnClick += HandleClick;
        }

        private void InitializeView()
        {
            ConcreteView.SetStats(levelStatsData.ElapsedTime, levelStatsData.TotalSteps);
            effectsHandler.PlayEffect(ParticleType.EndGameBurst);
        }

        protected override void OnClose()
        {
            ConcreteView.OnClick -= HandleClick;
        }

        private void HandleClick(EndGameView.BtnType btnType)
        {
            switch (btnType)
            {
                case EndGameView.BtnType.Restart:
                    WindowHandler.CloseWindow(WindowType.MazeGame);
                    WindowHandler.OpenWindow(WindowType.MazeGame, currentLevelData);
                    Close();
                    break;
                case EndGameView.BtnType.Exit:
                    WindowHandler.CloseWindow(WindowType.MazeGame);
                    WindowHandler.OpenWindow(WindowType.LevelChooser);
                    Close();
                    break;
            }
        }
    }
}