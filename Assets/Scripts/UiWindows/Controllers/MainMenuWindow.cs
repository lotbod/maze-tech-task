using Core.UiSystem;
using Core.UiSystem.Data;
using Core.UiSystem.Interfaces;
using Core.UiSystem.Pool;
using UiWindows.Views;

namespace UiWindows.Controllers
{
    public class MainMenuWindow : WindowController<MainMenuView>
    {
        public MainMenuWindow(WindowType type, IWindowHandler windowHandler, ViewPool viewPool) 
            : base(type, windowHandler, viewPool)
        {
        }
        
        protected override void OnOpen(object data = null)
        {
            ConcreteView.OnPlayClick += HandlePlayClick;
        }

        private void HandlePlayClick()
        {
            WindowHandler.OpenWindow(WindowType.LevelChooser);
            Close();
        }
    }
}
