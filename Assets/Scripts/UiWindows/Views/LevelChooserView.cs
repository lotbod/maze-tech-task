using System;
using Core.UiSystem;
using TMPro;
using UiWindows.Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace UiWindows.Views
{
    public class LevelChooserView : WindowView<LevelChooserWindow>
    {
        public enum BtnType
        {
            Random,
            ArrowLeft,
            ArrowRight,
            PlaySelected
        }
        public enum SliderType
        {
            Width,
            Height,
            Exits
        }
        
        public event Action<BtnType> OnClick;                                                                                                      
        public event Action<SliderType, float, float> OnSlider;                                                                                                      

        [SerializeField] private Button playRandomBtn;
        [SerializeField] private Button playSelectedBtn;
        [SerializeField] private Button arrowLeftBtn;
        [SerializeField] private Button arrowRightBtn;
        [SerializeField] private TMP_Text levelNameTmp;
        [SerializeField] private TMP_Text widthSliderTmp;
        [SerializeField] private TMP_Text heightSliderTmp;
        [SerializeField] private TMP_Text exitsSliderTmp;
        [SerializeField] private Slider widthSlider;
        [SerializeField] private Slider heightSlider;
        [SerializeField] private Slider exitsSlider;
        
        public override void OnPop()
        {
            playRandomBtn.onClick.AddListener(() => OnClick?.Invoke(BtnType.Random));
            playSelectedBtn.onClick.AddListener(() => OnClick?.Invoke(BtnType.PlaySelected));
            arrowLeftBtn.onClick.AddListener(() => OnClick?.Invoke(BtnType.ArrowLeft));
            arrowRightBtn.onClick.AddListener(() => OnClick?.Invoke(BtnType.ArrowRight));
            widthSlider.onValueChanged.AddListener(value => OnSlider?.Invoke(SliderType.Width, value, widthSlider.minValue));
            heightSlider.onValueChanged.AddListener(value => OnSlider?.Invoke(SliderType.Height, value, heightSlider.minValue));
            exitsSlider.onValueChanged.AddListener(value => OnSlider?.Invoke(SliderType.Exits, value, heightSlider.minValue));
        }

        public override void OnPush()
        {
            playRandomBtn.onClick.RemoveAllListeners();
            playSelectedBtn.onClick.RemoveAllListeners();
            arrowLeftBtn.onClick.RemoveAllListeners();
            arrowRightBtn.onClick.RemoveAllListeners();
            widthSlider.onValueChanged.RemoveAllListeners();
            heightSlider.onValueChanged.RemoveAllListeners();
            exitsSlider.onValueChanged.RemoveAllListeners();
        }

        public void SetLevelName(string text)
        {
            levelNameTmp.text = text;
        }

        public void DisableLevelsUi()
        {
            playSelectedBtn.interactable = false;
            arrowLeftBtn.gameObject.SetActive(false);
            arrowRightBtn.gameObject.SetActive(false);
        }
        
        public void EnableLevelsUi()
        {
            playSelectedBtn.interactable = true;
            arrowLeftBtn.gameObject.SetActive(true);
            arrowRightBtn.gameObject.SetActive(true);
        }

        public void SetButtonActive(BtnType btnType, bool state)
        {
            switch (btnType)
            {
                case BtnType.Random:
                    playRandomBtn.gameObject.SetActive(state);
                    break;
                case BtnType.ArrowLeft:
                    arrowLeftBtn.gameObject.SetActive(state);
                    break;
                case BtnType.ArrowRight:
                    arrowRightBtn.gameObject.SetActive(state);
                    break;
                case BtnType.PlaySelected:
                    playSelectedBtn.gameObject.SetActive(state);
                    break;
            }
        }

        public void SetSliderText(SliderType sliderType, string text)
        {
            switch (sliderType)
            {
                case SliderType.Width:
                    widthSliderTmp.text = text;
                    break;
                case SliderType.Height:
                    heightSliderTmp.text = text;
                    break;
                case SliderType.Exits:
                    exitsSliderTmp.text = text;
                    break;
            }
        }
    }
}