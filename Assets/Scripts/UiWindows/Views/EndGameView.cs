using System;
using Core.UiSystem;
using TMPro;
using UiWindows.Controllers;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UiWindows.Views
{
    public class EndGameView : WindowView<EndGameWindow>
    {
        public enum BtnType
        {
            Restart,
            Exit
        }
        
        public event Action<BtnType> OnClick;
        
        [SerializeField] private Button restartBtn;
        [SerializeField] private Button exitBtn;
        [SerializeField] private TMP_Text timeTmp;
        [SerializeField] private TMP_Text stepsTmp;

        public override void OnPop()
        {
            restartBtn.onClick.AddListener(() => OnClick?.Invoke(BtnType.Restart));
            exitBtn.onClick.AddListener(() => OnClick?.Invoke(BtnType.Exit));
        }

        public override void OnPush()
        {
            restartBtn.onClick.RemoveAllListeners();
            exitBtn.onClick.RemoveAllListeners();
        }

        public void SetStats(float time, int steps)
        {
            timeTmp.text = TimeSpan.FromSeconds(time).ToMinSecMilString();;
            stepsTmp.text = steps.ToString();
        }
    }
}