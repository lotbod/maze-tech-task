using System;
using Core.UiSystem;
using TMPro;
using UiWindows.Controllers;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UiWindows.Views
{
    public class MazeGameView : WindowView<MazeGameWindow>
    {
        public enum BtnType
        {
            Back,
            SaveLevel
        }
        
        public event Action<BtnType> OnClick;
        
        [SerializeField] private Button backBtn;
        [SerializeField] private Button saveLevelBtn;
        [SerializeField] private TMP_Text timerTmp;
        [SerializeField] private TMP_Text stepsTmp;

        public override void OnPop()
        {
            backBtn.onClick.AddListener(() => OnClick?.Invoke(BtnType.Back));
            saveLevelBtn.onClick.AddListener(() => OnClick?.Invoke(BtnType.SaveLevel));

            saveLevelBtn.interactable = true;
        }

        public override void OnPush()
        {
            backBtn.onClick.RemoveAllListeners();
            saveLevelBtn.onClick.RemoveAllListeners();
        }

        public void UpdateTimer(float time)
        {
            timerTmp.text = TimeSpan.FromSeconds(time).ToMinSecMilString();
        }

        public void UpdateSteps(int steps)
        {
            stepsTmp.text = steps.ToString();
        }

        public void StopSaveInteract()
        {
            saveLevelBtn.interactable = false;
        }
    }
}
