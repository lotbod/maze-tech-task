using System;
using Core.UiSystem;
using UiWindows.Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace UiWindows.Views
{
    public class MainMenuView : WindowView<MainMenuWindow>
    {
        public event Action OnPlayClick;
        
        [SerializeField] private Button startGameButton;

        public override void OnPop()
        {
            startGameButton.onClick.AddListener(HandlePlayButtonClick);
        }

        public override void OnPush()
        {
            startGameButton.onClick.RemoveAllListeners();
        }

        private void HandlePlayButtonClick()
        {
            OnPlayClick?.Invoke();
        }
    }
}