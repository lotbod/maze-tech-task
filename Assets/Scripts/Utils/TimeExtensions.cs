using System;

namespace Utils
{
    public static class TimeExtensions
    {
        public static string ToMinSecMilString(this TimeSpan timeSpan)
        {
            return timeSpan.ToString("mm':'ss':'ff");
        }
    }
}