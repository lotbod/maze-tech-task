using Core.UiSystem.Data;
using Core.UiSystem.Interfaces;
using UnityEngine;
using Zenject;

namespace Starters
{
    public class MainMenuStarter : MonoBehaviour
    {
        [Inject]
        private readonly IWindowHandler windowHandler;

        private void Start()
        {
            windowHandler.OpenWindow(WindowType.MainMenu);
        }
    }
}