using System.Collections.Generic;
using System.Linq;
using Maze.Data;
using Maze.Interfaces;
using UnityEngine;

namespace Maze
{
    public class MazeBuilder : IMazeBuilder
    {
        private const int RandomMinWidth = 5;
        private const int RandomMaxWidth = 20;
        private const int RandomMinHeight = 5;
        private const int RandomMaxHeight = 20;

        private readonly Vector2Int[] directions =
        {
            new(-1, 0),
            new(1, 0),
            new(0, 1),
            new(0, -1)
        };

        public MazeLevelData GenerateMaze(IMazeConfig config)
        {
            config.MazeWidth = config.MazeWidth == 0
                ? Random.Range(RandomMinWidth, RandomMaxWidth + 1)
                : config.MazeWidth;
            config.MazeHeight = config.MazeHeight == 0
                ? Random.Range(RandomMinHeight, RandomMaxHeight + 1)
                : config.MazeHeight;
            
            var startPos = GetCenter(config.MazeWidth, config.MazeHeight);
            var cells = MazeGenerationAlgo(config, startPos);
            var exits = GenerateExits(cells, config);
            
            return new MazeLevelData
            {
                Height = config.MazeHeight,
                Width = config.MazeWidth,
                StartPosition = startPos,
                ExitPositions = exits,
                Cells = cells
            };
        }

        private Dictionary<Vector2Int, CellData> MazeGenerationAlgo(IMazeConfig config, Vector2Int startPos)
        {
            var cells = new Dictionary<Vector2Int, CellData>(config.MazeWidth * config.MazeHeight);
            var unvisited = new HashSet<CellData>();
            var stackCells = new List<CellData>(config.MazeWidth * config.MazeHeight);

            for (int i = 1; i <= config.MazeWidth; i++)
            {
                for (int j = 1; j <= config.MazeHeight; j++)
                {
                    var cell = new CellData(i, j);
                    cells[new Vector2Int(i, j)] = cell;
                    unvisited.Add(cell);
                }
            }

            var currentCell = cells[startPos];
            unvisited.Remove(currentCell);

            while (unvisited.Count > 0)
            {
                var unvisitedNeighbours = GetUnvisitedNeighbours(cells, unvisited, currentCell.Position);

                if (unvisitedNeighbours.Count > 0)
                {
                    var checkCell = unvisitedNeighbours[Random.Range(0, unvisitedNeighbours.Count)];
                    stackCells.Add(currentCell);
                    CompareWalls(ref currentCell, ref checkCell);
                    currentCell = checkCell;
                    unvisited.Remove(currentCell);
                }
                else if (stackCells.Count > 0)
                {
                    currentCell = stackCells[^1];
                    stackCells.RemoveAt(stackCells.Count - 1);
                }
            }

            return cells;
        }

        private List<CellData> GetUnvisitedNeighbours(IReadOnlyDictionary<Vector2Int, CellData> cells,
            ICollection<CellData> unvisited, Vector2Int currPos)
        {
            var neighbours = new List<CellData>();
            if (cells.TryGetValue(currPos, out CellData nCell))
            {
                foreach (var pos in directions)
                {
                    Vector2Int neighbourPos = currPos + pos;
                    if (cells.TryGetValue(neighbourPos, out nCell) && unvisited.Contains(nCell))
                    {
                        neighbours.Add(nCell);
                    }
                }
            }

            return neighbours;
        }

        private void CompareWalls(ref CellData cCell, ref CellData nCell)
        {
            // If neighbour is left of current.
            if (nCell.Position.x < cCell.Position.x)
            {
                nCell.RemoveWall(CellData.Wall.Right);
                cCell.RemoveWall(CellData.Wall.Left);
            }
            // Else if neighbour is right of current.
            else if (nCell.Position.x > cCell.Position.x)
            {
                nCell.RemoveWall(CellData.Wall.Left);
                cCell.RemoveWall(CellData.Wall.Right);
            }
            // Else if neighbour is above current.
            else if (nCell.Position.y > cCell.Position.y)
            {
                nCell.RemoveWall(CellData.Wall.Bottom);
                cCell.RemoveWall(CellData.Wall.Top);
            }
            // Else if neighbour is below current.
            else if (nCell.Position.y < cCell.Position.y)
            {
                nCell.RemoveWall(CellData.Wall.Top);
                cCell.RemoveWall(CellData.Wall.Bottom);
            }
        }

        private Vector2Int[] GenerateExits(IReadOnlyDictionary<Vector2Int, CellData> cells, IMazeConfig config)
        {
            List<CellData> GetEdgeCells()
            {
                return cells.Values
                    .Where(cell => cell.Position.x == 1 || cell.Position.x == config.MazeWidth || 
                                   cell.Position.y == 1 || cell.Position.y == config.MazeHeight)
                    .ToList();
            }

            var edgeCells = GetEdgeCells();
            var exitPositions = new Vector2Int[config.ExitsCount];

            for (int i = 0; i < exitPositions.Length; i++)
            {
                var cell = edgeCells[Random.Range(0, edgeCells.Count)];
                var position = cell.Position;

                if (cell.Position is { x: 1, y: 1 })
                {
                    int randomWall = Random.Range(0, 2);
                    if (randomWall == 0)
                    {
                        cells[position].RemoveWall(CellData.Wall.Left);
                        position.x--;
                    }
                    else
                    {
                        cells[position].RemoveWall(CellData.Wall.Bottom);
                        position.y--;
                    }
                }
                else if (cell.Position.x == config.MazeWidth && cell.Position.y == config.MazeHeight)
                {
                    int randomWall = Random.Range(0, 2);
                    if (randomWall == 0)
                    {
                        cells[position].RemoveWall(CellData.Wall.Right);
                        position.x++;
                    }
                    else
                    {
                        cells[position].RemoveWall(CellData.Wall.Top);
                        position.y++;
                    }
                }
                else if (cell.Position.x == 1 && cell.Position.y == config.MazeHeight)
                {
                    int randomWall = Random.Range(0, 2);
                    if (randomWall == 0)
                    {
                        cells[position].RemoveWall(CellData.Wall.Top);
                        position.y++;
                    }
                    else
                    {
                        cells[position].RemoveWall(CellData.Wall.Left);
                        position.x--;
                    }
                }
                else if (cell.Position.x == config.MazeWidth && cell.Position.y == 1)
                {
                    int randomWall = Random.Range(0, 2);
                    if (randomWall == 0)
                    {
                        cells[position].RemoveWall(CellData.Wall.Bottom);
                        position.y--;
                    }
                    else
                    {
                        cells[position].RemoveWall(CellData.Wall.Right);
                        position.x++;
                    }
                }
                else if(cell.Position is { x: 1, y: > 1 })
                {
                    cells[position].RemoveWall(CellData.Wall.Left);
                    position.x--;
                }
                else if(cell.Position is { x: > 1, y: 1 })
                {
                    cells[position].RemoveWall(CellData.Wall.Bottom);
                    position.y--;
                }
                else if(cell.Position.x == config.MazeWidth && cell.Position.y > 1)
                {
                    cells[position].RemoveWall(CellData.Wall.Right);
                    position.x++;
                }
                else if(cell.Position.x > 1 && cell.Position.y == config.MazeHeight)
                {
                    cells[position].RemoveWall(CellData.Wall.Top);
                    position.y++;
                }

                exitPositions[i] = position;
                edgeCells.Remove(cell);
            }

            return exitPositions;
        }

        private Vector2Int GetCenter(int width, int height)
        {
            return new Vector2Int(width / 2 + 1, height / 2 + 1);
        }
    }
}