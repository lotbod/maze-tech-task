using Maze.Interfaces;

namespace Maze
{
    public class MazeConfiguration : IMazeConfig
    {
        public int MazeWidth { get; set; }
        public int MazeHeight { get; set; }
        public int ExitsCount { get; set; } = 1;
    }
}
