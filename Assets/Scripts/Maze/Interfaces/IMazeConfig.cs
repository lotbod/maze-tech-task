namespace Maze.Interfaces
{
    public interface IMazeConfig
    {
        int MazeWidth { get; set; }
        int MazeHeight { get; set; }
        int ExitsCount { get; set; }
    }
}