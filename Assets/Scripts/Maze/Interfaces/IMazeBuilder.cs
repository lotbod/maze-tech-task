using Maze.Data;

namespace Maze.Interfaces
{
    public interface IMazeBuilder
    {
        MazeLevelData GenerateMaze(IMazeConfig config);
    }
}