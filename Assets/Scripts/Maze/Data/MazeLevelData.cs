using System.Collections.Generic;
using UnityEngine;

namespace Maze.Data
{
    public struct MazeLevelData
    {
        public Dictionary<Vector2Int, CellData> Cells;
        public Vector2Int[] ExitPositions;
        public Vector2Int StartPosition; 
        public int Width;
        public int Height;
    }

    public struct CellData
    {
        public enum Wall
        {
            Top,
            Right,
            Bottom,
            Left
        }
        
        public Vector2Int Position;
        public HashSet<Wall> Walls;

        public CellData(int x, int y)
        {
            Position = new Vector2Int(x, y);
            Walls = new HashSet<Wall> { Wall.Top, Wall.Right, Wall.Bottom, Wall.Left };
        }

        public void RemoveWall(Wall wall)
        {
            Walls.Remove(wall);
        }
    }
}