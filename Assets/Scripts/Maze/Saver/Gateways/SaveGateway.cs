using System;
using System.IO;
using Maze.Saver.Data;
using UnityEngine;

namespace Maze.Saver.Gateways
{
    public class SaveGateway : ISaveGateway
    {
        private readonly string folderPath = Path.Combine(Application.dataPath, "MazeLevels");

        public string[] GetAllLevelsNames()
        {
            string[] result;
            
            if (Directory.Exists(folderPath))
            {
                string[] fileNames = Directory.GetFiles(folderPath, "*.json");
                result = new string[fileNames.Length];
                
                if (fileNames.Length > 0)
                {
                    for (var i = 0; i < fileNames.Length; i++)
                    {
                        result[i] = Path.GetFileNameWithoutExtension(fileNames[i]);
                    }
                }
            }
            else
            {
                result = Array.Empty<string>();
            }

            return result;
        }
        
        public void SaveLevel(LevelData levelData, string name)
        {
            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            if (string.IsNullOrEmpty(name))
            {
                var files = Directory.GetFiles(folderPath, "*.json");
                name = $"Level_{files.Length + 1}";
            }
            
            string filePath = Path.Combine(folderPath, $"{name}.json");
            string json = JsonUtility.ToJson(levelData);
            File.WriteAllText(filePath, json);
        }

        public bool TryToLoadLevel(string name, out LevelData levelData)
        {
            string filePath = Path.Combine(folderPath, $"{name}.json");
            levelData = default;
            
            if (!File.Exists(filePath)) return false;

            string json = File.ReadAllText(filePath);
            levelData = JsonUtility.FromJson<LevelData>(json);
            
            return true;
        }
    }
}
