using Maze.Saver.Data;

namespace Maze.Saver.Gateways
{
    public interface ISaveGateway
    {
        void SaveLevel(LevelData levelData, string name);
        bool TryToLoadLevel(string name, out LevelData levelData);
        string[] GetAllLevelsNames();
    }
}