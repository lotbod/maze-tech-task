using System;
using UnityEngine;

namespace Maze.Saver.Data
{
    [Serializable]
    public struct CellData
    {
        public Vector2Int pos;
        public int[] walls;
    }
    
    [Serializable]
    public struct LevelData
    {
        public CellData[] cells;
        public Vector2Int[] exitPositions;
        public Vector2Int startPosition; 
        public int width;
        public int height;
    }
}