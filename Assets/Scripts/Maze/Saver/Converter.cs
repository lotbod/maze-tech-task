using System.Collections.Generic;
using System.Linq;
using Maze.Data;
using Maze.Saver.Data;
using UnityEngine;
using CellData = Maze.Saver.Data.CellData;

namespace Maze.Saver
{
    public static class Converter
    {
        public static LevelData ConvertToLevelData(MazeLevelData mazeData)
        {
            var levelData = new LevelData();
            levelData.cells = new CellData[mazeData.Cells.Values.Count];
            
            int i = 0;
            foreach (var value in mazeData.Cells.Values)
            {
                levelData.cells[i] = new CellData { pos = value.Position,
                    walls = value.Walls.Cast<int>().ToArray() };
                i++;
            }
            
            levelData.width = mazeData.Width;
            levelData.height = mazeData.Height;
            levelData.startPosition = mazeData.StartPosition;
            levelData.exitPositions = mazeData.ExitPositions;

            return levelData;
        }
        
        public static MazeLevelData ConvertToMazeData(LevelData levelData)
        {
            var mazeData = new MazeLevelData();
            mazeData.Cells = new Dictionary<Vector2Int, Maze.Data.CellData>();
            
            for (int i = 0; i < levelData.cells.Length; i++)
            {
                var cell = levelData.cells[i];
                mazeData.Cells[new Vector2Int(cell.pos.x, cell.pos.y)] = new Maze.Data.CellData
                {
                    Position = cell.pos,
                    Walls = cell.walls.Select(x => (Maze.Data.CellData.Wall)x).ToHashSet()
                };
            }

            mazeData.Width = levelData.width;
            mazeData.Height = levelData.height;
            mazeData.StartPosition = levelData.startPosition;
            mazeData.ExitPositions = levelData.exitPositions;
            
            return mazeData;
        }
    }
}