using Maze.Data;

namespace Maze.Saver.UseCases
{
    using Gateways;
    
    public class TryToLoadLevelUseCase
    {
        private readonly ISaveGateway saveGateway;

        public TryToLoadLevelUseCase(ISaveGateway saveGateway)
        {
            this.saveGateway = saveGateway;
        }

        public bool TryToLoadLevel(string name, out MazeLevelData levelData)
        {
            levelData = default;

            if (!saveGateway.TryToLoadLevel(name, out var data)) 
                return false;
            
            levelData = Converter.ConvertToMazeData(data);
            return true;
        }
    }
}