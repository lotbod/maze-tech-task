namespace Maze.Saver.UseCases
{
    using Gateways;
    
    public class GetLevelsNamesUseCase
    {
        private readonly ISaveGateway saveGateway;

        public GetLevelsNamesUseCase(ISaveGateway saveGateway)
        {
            this.saveGateway = saveGateway;
        }

        public string[] GetNames()
        {
            return saveGateway.GetAllLevelsNames();
        }
    }
}