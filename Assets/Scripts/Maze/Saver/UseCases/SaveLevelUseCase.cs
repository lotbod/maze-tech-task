using Maze.Data;

namespace Maze.Saver.UseCases
{
    using Gateways;
    using Data;
    
    public class SaveLevelUseCase
    {
        private readonly ISaveGateway saveGateway;

        public SaveLevelUseCase(ISaveGateway saveGateway)
        {
            this.saveGateway = saveGateway;
        }

        public void SaveLevel(MazeLevelData levelData, string name = "")
        {
            var convertedData = Converter.ConvertToLevelData(levelData);
            saveGateway.SaveLevel(convertedData, name);
        }
    }
}