using System.Collections.Generic;
using UnityEngine;

namespace Game.Effects
{
    public class EffectsMonoHandler : MonoBehaviour, IEffectsHandler
    {
        [SerializeField] private ParticleSystem backgroundPs;
        [SerializeField] private ParticleSystem endGamePs;

        private readonly Dictionary<ParticleType, ParticleSystem> particleSystems = new ();

        private void Awake()
        {
            particleSystems.Add(ParticleType.MazeBackground, backgroundPs);
            particleSystems.Add(ParticleType.EndGameBurst, endGamePs);
        }

        public void PlayEffect(ParticleType particleType)
        {
            if(particleSystems.TryGetValue(particleType, out var particle))
                particle.Play();
        }
        
        public void StopEffect(ParticleType particleType)
        {
            if(particleSystems.TryGetValue(particleType, out var particle))
                particle.Stop();
        }
    }
}
