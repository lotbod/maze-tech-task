namespace Game.Effects
{
    public enum ParticleType
    {
        MazeBackground,
        EndGameBurst
    }
}