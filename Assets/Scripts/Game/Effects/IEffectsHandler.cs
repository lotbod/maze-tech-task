namespace Game.Effects
{
    public interface IEffectsHandler
    {
        void PlayEffect(ParticleType particleType);
        void StopEffect(ParticleType particleType);
    }
}