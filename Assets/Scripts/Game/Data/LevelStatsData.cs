namespace Game.Data
{
    public struct LevelStatsData
    {
        public float ElapsedTime;
        public int TotalSteps;
    }
}