using System.Collections.Generic;
using Cinemachine;
using Game.Player;
using Maze.Data;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace Game.Map
{
    public class MapMonoHandler : MonoBehaviour, IMapHandler
    {
        [SerializeField] private Transform mazeMap;
        [SerializeField] private CinemachineVirtualCamera virtualCamera;
        
        [FormerlySerializedAs("playerPrefab")] [SerializeField] private PlayerView playerViewPrefab;
        [SerializeField] private Cell cellPrefab;
        [SerializeField] private GameObject flagPrefab;

        private readonly List<GameObject> spawnedObjects = new ();

        [Inject] private IPlayerHandler playerHandler;

        private void Start()
        {
            gameObject.SetActive(false);
        }

        public void BuildMap(MazeLevelData levelData)
        {
            foreach (var cellData in levelData.Cells.Values)
            {
                var cell = Instantiate(cellPrefab, mazeMap);
                cell.transform.position = new Vector3(cellData.Position.x, cellData.Position.y);
                cell.Initialize(cellData);
                spawnedObjects.Add(cell.gameObject);
            }

            foreach (var exitPosition in levelData.ExitPositions)
            {
                var flag = Instantiate(flagPrefab, mazeMap);
                flag.transform.position = new Vector3(exitPosition.x, exitPosition.y);
                spawnedObjects.Add(flag);
            }

            var player = Instantiate(playerViewPrefab, mazeMap);
            playerHandler.SetData(player, levelData);
            spawnedObjects.Add(player.gameObject);

            virtualCamera.m_Follow = player.transform;
            //virtualCamera.m_Lens.OrthographicSize = Mathf.Pow(levelData.Width / 3f + levelData.Height / 2f, 0.7f) + 1;

            gameObject.SetActive(true);
        }

        public void ClearMap()
        {
            foreach (var obj in spawnedObjects)
            {
                Destroy(obj);
            }

            playerHandler.ClearData();
            gameObject.SetActive(false);
        }
    }
}
