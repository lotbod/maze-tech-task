using Maze.Data;
using UnityEngine;

namespace Game.Map
{
    public class Cell : MonoBehaviour
    {
        [SerializeField] private GameObject leftWall;
        [SerializeField] private GameObject rightWall;
        [SerializeField] private GameObject topWall;
        [SerializeField] private GameObject bottomWall;

        public void Initialize(CellData data)
        {
            leftWall.SetActive(false);
            rightWall.SetActive(false);
            topWall.SetActive(false);
            bottomWall.SetActive(false);
            
            foreach (var wall in data.Walls)
            {
                switch (wall)
                {
                    case CellData.Wall.Top:
                        topWall.SetActive(true);
                        break;
                    case CellData.Wall.Right:
                        rightWall.SetActive(true);
                        break;
                    case CellData.Wall.Bottom:
                        bottomWall.SetActive(true);
                        break;
                    case CellData.Wall.Left:
                        leftWall.SetActive(true);
                        break;
                }
            }
        }
    }
}