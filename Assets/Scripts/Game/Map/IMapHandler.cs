using Maze.Data;

namespace Game.Map
{
    public interface IMapHandler
    {
        void BuildMap(MazeLevelData levelData);
        void ClearMap();
    }
}