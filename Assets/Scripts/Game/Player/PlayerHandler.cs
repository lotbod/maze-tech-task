using System;
using System.Linq;
using Cysharp.Threading.Tasks;
using Maze.Data;
using UnityEngine;

namespace Game.Player
{
    public class PlayerHandler : IPlayerHandler
    {
        public event Action OnStart;
        public event Action OnEnd;
        public event Action OnStep;

        private PlayerView currPlayerView;
        private MazeLevelData currLevelData;
        private Vector2Int currPos;

        private bool isStarted;
        private UniTaskCompletionSource completionSource;

        public void SetData(PlayerView playerView, MazeLevelData levelData)
        {
            currPlayerView = playerView;
            currLevelData = levelData;
            currPos = levelData.StartPosition;

            playerView.Initialize(levelData.StartPosition);
            playerView.StartInput();
            playerView.OnMove += MoveTo;
        }

        public void ClearData()
        {
            if (currPlayerView == null) return;

            currPlayerView.StopInput();
            currPlayerView.OnMove -= MoveTo;
            currPlayerView = null;
            isStarted = false;
        }

        private void MoveTo(Vector2Int pos)
        {
            if (!CanMoveTo(pos)) return;

            if (!isStarted)
            {
                OnStart?.Invoke();
                isStarted = true;
            }

            OnStep?.Invoke();

            currPos += pos;
            currPlayerView.MoveTo(new Vector3(currPos.x, currPos.y), CheckLevelComplete);
        }

        private bool CanMoveTo(Vector2Int pos)
        {
            if (currLevelData.Cells.TryGetValue(currPos, out var cell))
            {
                if (pos == Vector2Int.up && !cell.Walls.Contains(CellData.Wall.Top)) return true;
                if (pos == Vector2Int.down && !cell.Walls.Contains(CellData.Wall.Bottom)) return true;
                if (pos == Vector2Int.left && !cell.Walls.Contains(CellData.Wall.Left)) return true;
                if (pos == Vector2Int.right && !cell.Walls.Contains(CellData.Wall.Right)) return true;
            }

            return false;
        }

        private void CheckLevelComplete()
        {
            if (currLevelData.ExitPositions.Contains(currPos) && isStarted)
            {
                OnEnd?.Invoke();
                ClearData();
            }
        }
    }
}