using System;
using System.Collections;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

namespace Game.Player
{
    public class PlayerView : MonoBehaviour
    {
        public event Action<Vector2Int> OnMove;
        
        private Coroutine inputCoroutine;
        
        private const float MoveDuration = 0.13f;

        public void Initialize(Vector2Int startPos)
        {
            transform.position = new Vector3(startPos.x, startPos.y);
        }

        public void MoveTo(Vector3 nextPos, Action onComplete = null)
        {
            transform.DOMove(nextPos, MoveDuration).OnComplete(() => onComplete?.Invoke());
        }

        public async void StartInput()
        {
            await UniTask.WaitUntil(() => gameObject.activeSelf);
            inputCoroutine = StartCoroutine(InputCoroutine());
        }

        public void StopInput()
        {
            if (inputCoroutine != null)
                StopCoroutine(inputCoroutine);
        }

        private IEnumerator InputCoroutine()
        {
            while (this)
            {
                if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    OnMove?.Invoke(Vector2Int.left);
                }
                else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
                {
                    OnMove?.Invoke(Vector2Int.right);
                }
                else if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
                {
                    OnMove?.Invoke(Vector2Int.up);
                }
                else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
                {
                    OnMove?.Invoke(Vector2Int.down);
                }

                yield return null;
            }
        }
    }
}