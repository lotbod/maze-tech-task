using System;
using Maze.Data;

namespace Game.Player
{
    public interface IPlayerHandler
    {
        event Action OnStart;
        event Action OnEnd;
        event Action OnStep;
        void SetData(PlayerView playerView, MazeLevelData levelData);
        void ClearData();
    }
}