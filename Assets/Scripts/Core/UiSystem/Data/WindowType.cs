﻿namespace Core.UiSystem.Data
{
    public enum WindowType
    {
        None = 0,
        MainMenu = 100,
        LevelChooser = 200,
        MazeGame = 300,
        EndGame = 400,
        MazeGenerator = 500
    }
}